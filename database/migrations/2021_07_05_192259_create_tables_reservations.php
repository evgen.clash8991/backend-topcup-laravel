<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables_reservations', function (Blueprint $table) {
            $table->id();
            $table->integer('table_id');
            $table->integer('user_id');
            $table->string('phone_number');
            $table->timestamp('booked_at')->useCurrent();
            $table->timestamp('book_start');
            $table->timestamp('book_end');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables_reservations');
    }
}
